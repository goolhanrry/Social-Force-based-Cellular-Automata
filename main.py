# -*- coding: utf-8 -*-

import SFCA

# build Cellular Automata model
ca = SFCA.CellularAutomata()
ca.generate_agents()

# run model
ca.evolve()

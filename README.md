# Social-Force-based-Cellular-Automata

[![License](https://img.shields.io/badge/license-MIT-blue.svg)](LICENSE)

<p align="center">
  <img width="800" src="https://github.com/goolhanrry/Social-Force-based-Cellular-Automata/blob/master/assets/Screenshot.png">
</p>

# -*- coding: utf-8 -*-

import utils
import numpy as np
import matplotlib.pyplot as plt


class CellularAutomata:
    def __init__(self):
        self.agents = []

    def generate_agents(self):
        '''
            Generate agents for the model.
        '''

        while len(self.agents) < utils.args['num_of_agents']:
            pt = [np.random.randint(0, utils.floors[0]['width']), np.random.randint(0, utils.floors[0]['height']), 0, len(self.agents)]
            if self.verify_coordinate(pt):
                self.agents.append(pt)

    def verify_coordinate(self, pt):
        '''
            Verify validation of the point.

            Parameters:
                pt        - Point to be verified

            Return:
                True      - Valid
                False     - Invalid
        '''

        # return false if point is out of range
        if pt[0] <= 0 or pt[0] >= utils.floors[0]['width'] or pt[1] <= 0 or pt[1] >= utils.floors[0]['height']:
            return False

        def get_cross(pt1, pt2, pt):
            return (pt2[0] - pt1[0]) * (pt[1] - pt1[1]) - (pt[0] - pt1[0]) * (pt2[1] - pt1[1])

        # check whether the point is in accessible area
        for pt1, pt2, pt3, pt4 in utils.floors[0]['boundary']:
            if get_cross(pt1, pt2, pt) * get_cross(pt3, pt4, pt) >= 0 and get_cross(pt2, pt3, pt) * get_cross(pt4, pt1, pt) >= 0:
                return False

        return True

    def plot(self):
        '''
            Plot the current evolution result.
        '''

        # init axes
        plt.clf()
        ax = plt.gca()
        ax.set_aspect('equal')

        # draw walls
        walls = np.array(utils.floors[0]['walls'])
        ax.plot(walls[:, :, 0].T, walls[:, :, 1].T, color=(0, 0, 0), linewidth=1)

        # draw unaccessible area
        for area in utils.floors[0]['unaccessible_area']:
            plt.fill(area[0], area[1], facecolor=(0.7, 0.7, 0.7))

        # draw exits
        for exit in utils.floors[0]['exits']:
            plt.fill(exit[0], exit[1], facecolor=(0, 1, 0.6))

        # draw agents
        if len(self.agents):
            agents = np.array(self.agents)
            plt.scatter(agents[:, 0], agents[:, 1], s=10, color=(1, 0.53, 0.3))

        # hide axis
        plt.axis('off')

    def evolve(self):
        '''
            Evolve the Cellular Automata model
        '''

        # start interactive mode
        plt.ion()

        # evolve until all agents exit
        sec = 0
        while len(self.agents):
            next_mtx = np.zeros((utils.floors[0]['width'], utils.floors[0]['height']))

            for index, agent in enumerate(self.agents):
                # search for nearest exit
                if (agent[2] < 3 or agent[2] > 75):
                    dist = utils.get_dist_list(agent, 0)
                    min_index = utils.get_min_index(dist)
                else:
                    self.agents[index][2] += 1
                    min_index = 1

                # delete exited agent
                if abs(utils.floors[0]['exit_points'][min_index][0] - agent[0]) <= utils.args['step_size'] and abs(utils.floors[0]['exit_points'][min_index][1] - agent[1]) <= utils.args['step_size']:
                    print('Agent No.{0} escaped. Time: {1}s'.format(agent[3], sec))
                    self.agents.pop(index)
                    continue

                #  add noise to model
                if np.random.rand() < 1 - utils.args['noise_ratio']:
                    if utils.floors[0]['exit_points'][min_index][0] > agent[0]:
                        next_x = agent[0] + utils.args['step_size']
                    elif utils.floors[0]['exit_points'][min_index][0] < agent[0]:
                        next_x = agent[0] - utils.args['step_size']
                    else:
                        next_x = agent[0]

                    if utils.floors[0]['exit_points'][min_index][1] > agent[1]:
                        next_y = agent[1] + utils.args['step_size']
                    elif utils.floors[0]['exit_points'][min_index][1] < agent[1]:
                        next_y = agent[1] - utils.args['step_size']
                    else:
                        next_y = agent[1]
                else:
                    next_x = agent[0] + utils.args['noise_level'] - np.random.randint(0, 2 * utils.args['noise_level'])
                    next_y = agent[1] + utils.args['noise_level'] - np.random.randint(0, 2 * utils.args['noise_level'])

                if self.verify_coordinate((next_x, next_y)):
                    pass
                elif self.verify_coordinate((agent[0], next_y)):
                    next_x = agent[0]
                elif self.verify_coordinate((next_x, agent[1])):
                    next_y = agent[1]
                else:
                    next_x = agent[0]
                    next_y = agent[1]

                if not next_mtx[next_x][next_y] or (abs(utils.floors[0]['exit_points'][min_index][0] - next_x) <= 5 and abs(utils.floors[0]['exit_points'][min_index][1] - next_y) <= 5):
                    self.agents[index] = [next_x, next_y, agent[2], agent[3]]
                    next_mtx[next_x][next_y] = 1

                if abs(next_x - agent[0]) <= utils.args['step_size'] / 2 and abs(next_y - agent[1]) <= utils.args['step_size'] / 2:
                    self.agents[index][2] += 1

            sec += 1

            self.plot()
            plt.pause(0.001)

        print('All agents escaped successfully. Total Time: {0}s'.format(sec))

        # stop interactive mode
        plt.ioff()
